package proyectofinalempresa;

public class Empleado {

    private int cc_empleado;
    private String nombre_empleado;
    private int ventas_empleado;

    public Empleado(int cc_empleado, String nombre_empleado, int ventas_empleado) {
        this.cc_empleado = cc_empleado;
        this.nombre_empleado = nombre_empleado;
        this.ventas_empleado = ventas_empleado;
    }

    public Empleado() {
        this.cc_empleado = 0;
        this.nombre_empleado = "";
        this.ventas_empleado = 0;
    }

    public int getCc_empleado() {
        return cc_empleado;
    }

    public String getNombre_empleado() {
        return nombre_empleado;
    }

    public int getVentas_empleado() {
        return ventas_empleado;
    }

    public void setCc_empleado(int cc_empleado) {
        this.cc_empleado = cc_empleado;
    }

    public void setNombre_empleado(String nombre_empleado) {
        this.nombre_empleado = nombre_empleado;
    }

    public void setVentas_empleado(int ventas_empleado) {
        this.ventas_empleado = ventas_empleado;
    }

    @Override
    public String toString() {
        return "Empleado{" + "cc_empleado=" + cc_empleado + ", nombre_empleado=" + nombre_empleado + ", ventas_empleado=" + ventas_empleado + '}';
    }

}
