package proyectofinalempresa;

public class Sede {

    private int id_sede;
    private String nombre_sede;
    private ListaELC emple;

    public Sede() {
        this.id_sede =0;
        this.nombre_sede = "";
        this.emple = null;
    }

    public Sede(int id_sede, String nombre_sede, ListaELC emple) {
        this.id_sede = id_sede;
        this.nombre_sede = nombre_sede;
        this.emple = emple;
    }

    public int getId_sede() {
        return id_sede;
    }

    public String getNombre_sede() {
        return nombre_sede;
    }

    public ListaELC getEmple() {
        return emple;
    }

    public void setId_sede(int id_sede) {
        this.id_sede = id_sede;
    }

    public void setNombre_sede(String nombre_sede) {
        this.nombre_sede = nombre_sede;
    }

    public void setEmple(ListaELC emple) {
        this.emple = emple;
    }
     
    

    @Override
    public String toString() {
        return "Sede{" + "id_sede=" + id_sede + ", nombre_sede=" + nombre_sede + ", Ventas="+emple.contadorventasemple()+" emple=" + emple.Most_Ini_Ult() + '}';
    }

   

}
