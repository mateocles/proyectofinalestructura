package proyectofinalempresa;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author decodigo
 */
public class LectorArchivos {

    private ArbolBinario AB = new ArbolBinario();

    public ArbolBinario getAB() {
        return AB;
    }

    public void leerDocumentosExcel(String rutaEmpleados, String rutaSedes, String rutaCiudades) {
        try {
            //////
            //     
            FileInputStream inputStream = new FileInputStream(new File(rutaEmpleados));
            Workbook workbook = new XSSFWorkbook(inputStream);//libro
            int nHojas = workbook.getNumberOfSheets(); //Obtenemos el número de hojas que contiene el documento            /////////////////////
            for (int i = 0; i < nHojas; i++) {//recorre el numero de hojas
                ListaELC ELC = new ListaELC();
                Sheet hoja = workbook.getSheetAt(i);//salta paginas del excel
                Iterator iteradorFila = hoja.iterator();//iterador fila
                while (iteradorFila.hasNext()) {
                    Row siguienteFila = (Row) iteradorFila.next();//salta las filas de la tabla
                    Iterator iteradorColumnas = siguienteFila.cellIterator();
                    String eNombre = "";
                    int eId = 0, eVentas = 0;
                    while (iteradorColumnas.hasNext()) {//columna
                        Cell celda = (Cell) iteradorColumnas.next();
                        switch (celda.getColumnIndex()) {//cada una de las celdas
                            case 0:
                                if (celda.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                    eId = (int) Math.round(celda.getNumericCellValue());
                                }
                                break;
                            case 1:
                                if (celda.getCellType() == Cell.CELL_TYPE_STRING) {
                                    eNombre = celda.getStringCellValue();
                                }
                                break;
                            case 2:
                                if (celda.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                    eVentas = (int) Math.round(celda.getNumericCellValue());
                                }
                                break;
                        }
                    }
                    ELC.Inser_Ini(new Empleado(eId, eNombre, eVentas));
                }
                insertarSedes(rutaSedes, rutaCiudades, ELC);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void insertarSedes(String rutaSedes, String rutaCiudades, ListaELC listaLEmpleados) {
        try {
            ///

            ///Continuamos con las Sedes    
            FileInputStream inputStream2 = new FileInputStream(new File(rutaSedes));
            Workbook workbook2 = new XSSFWorkbook(inputStream2);//libro
            int nHojas2 = workbook2.getNumberOfSheets(); //Obtenemos el número de hojas que contiene el documento
            ///Recorre el excel de sedes

            for (int j = 0; j < nHojas2; j++) {//recorre el numero de hojas
                ListaSLC listaSede1 = new ListaSLC();
                Sheet hoja2 = workbook2.getSheetAt(j);//salta paginas del excel
                Iterator iteradorFila2 = hoja2.iterator();//iterador fila
                while (iteradorFila2.hasNext()) {//fila
                    String sNombre = "";
                    int sId = 0;
                    Row siguienteFila = (Row) iteradorFila2.next();//salta las filas de la tabla
                    //para que no considere(omita) el encabezado de cada tabla
                    Iterator iteradorColumnas = siguienteFila.cellIterator();
                    while (iteradorColumnas.hasNext()) {//columna

                        Cell celda = (Cell) iteradorColumnas.next();
                        switch (celda.getColumnIndex()) {//cada una de las celdas
                            case 0:
                                if (celda.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                    sId = (int) Math.round(celda.getNumericCellValue());
                                }
                                break;
                            case 1:
                                if (celda.getCellType() == Cell.CELL_TYPE_STRING) {
                                    sNombre = celda.getStringCellValue();
                                }
                                break;
                        }//fin columna
                    }
                    listaSede1.InsertarInicio(new Sede(sId, sNombre, listaLEmpleados));
                    //insertamos la sede a la lista de sedes
                }//fin filas
                insertarCiudades(rutaCiudades, listaSede1);
            }//fin hoja
            //////////////////////////////////////////////////////////////////////////////
            //falta programar
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void insertarCiudades(String rutaCiudades, ListaSLC listaSedes2) {
        ///
        try {

            FileInputStream inputStream3 = new FileInputStream(new File(rutaCiudades));
            Workbook workbook3 = new XSSFWorkbook(inputStream3);//libro
            int nHojas3 = workbook3.getNumberOfSheets(); //Obtenemos el número de hojas que contiene el documento
            ///Recorre el excel de sedes

            for (int j = 0; j < nHojas3; j++) {//recorre el numero de hojas
                Sheet hoja3 = workbook3.getSheetAt(j);//salta paginas del excel
                Iterator iteradorFila2 = hoja3.iterator();//iterador fila
                String cNombre = "";
                int cId = 0;
                while (iteradorFila2.hasNext()) {//fila
                    Row siguienteFila = (Row) iteradorFila2.next();//salta las filas de la tabla
                    Iterator iteradorColumnas = siguienteFila.cellIterator();
                    while (iteradorColumnas.hasNext()) {//columna
                        Cell celda = (Cell) iteradorColumnas.next();
                        switch (celda.getColumnIndex()) {//cada una de las celdas
                            case 0:
                                if (celda.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                                    cId = (int) Math.round(celda.getNumericCellValue());
                                }
                                break;
                            case 1:
                                if (celda.getCellType() == Cell.CELL_TYPE_STRING) {
                                    cNombre = celda.getStringCellValue();
                                }
                                break;

                        }//fin columna
                    }
                    AB.insert(new Ciudad(cId, cNombre, listaSedes2));
                }//fin fila

            }//fin hoja

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
