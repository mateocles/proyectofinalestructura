package proyectofinalempresa;

public class NodoELC {

    private NodoELC siguiente;
    private NodoELC anterior;
    private Empleado emple;

    public NodoELC(NodoELC siguiente, NodoELC anterior, Empleado emple) {
        this.siguiente = siguiente;
        this.anterior = anterior;
        this.emple = emple;
    }

    public NodoELC getSiguiente() {
        return siguiente;
    }

    public NodoELC getAnterior() {
        return anterior;
    }

    public Empleado getEmple() {
        return emple;
    }

    public void setSiguiente(NodoELC siguiente) {
        this.siguiente = siguiente;
    }

    public void setAnterior(NodoELC anterior) {
        this.anterior = anterior;
    }

    public void setEmple(Empleado emple) {
        this.emple = emple;
    }

    @Override
    public String toString() {
        return "NodoELC{" + "siguiente=" + siguiente + ", anterior=" + anterior + ", emple=" + emple + '}';
    }

}
