package proyectofinalempresa;

public class NodoAB {

    private Ciudad city;  
    private NodoAB izq;
    private NodoAB dere;

    public NodoAB(Ciudad city) {
        this.city = city;
        izq = dere = null;
    }

    public Ciudad getCiudad() {
        return city;
    }

    public NodoAB getIzq() {
        return izq;
    }

    public NodoAB getDere() {
        return dere;
    }

    public void setDato(Ciudad city) {
        this.city = city;
    }

    public void setIzq(NodoAB izq) {
        this.izq = izq;
    }

    public void setDere(NodoAB dere) {
        this.dere = dere;
    }

    @Override
    public String toString() {
        return "Nodo{" + "dato=" +  city + ", izq=" + izq + ", dere=" + dere + '}';
    }

}
