package proyectofinalempresa;

public class Ciudad {

    private int id_ciudad;
    private String nombre_ciudad;
    private ListaSLC sedes;//lista ciruclar

    public Ciudad() {
        this.id_ciudad = 0;
        this.nombre_ciudad = "";
        this.sedes = null;
    }

    public Ciudad(int id_ciudad, String nombre_ciudad, ListaSLC sedes) {
        this.id_ciudad = id_ciudad;
        this.nombre_ciudad = nombre_ciudad;
        this.sedes = sedes;
    }

    public int getId_ciudad() {
        return id_ciudad;
    }

    public String getNombre_ciudad() {
        return nombre_ciudad;
    }

    public ListaSLC getSedes() {
        return sedes;
    }

    public void setId_ciudad(int id_ciudad) {
        this.id_ciudad = id_ciudad;
    }

    public void setNombre_ciudad(String nombre_ciudad) {
        this.nombre_ciudad = nombre_ciudad;
    }

    public void setSedes(ListaSLC sedes) {
        this.sedes = sedes;
    }

    @Override
    public String toString() {
        return "Ciudad{" + "id_ciudad=" + id_ciudad + ", nombre_ciudad=" + nombre_ciudad + " Ventas="+sedes.contadordeventasxporcuidades()+", sedes=" + sedes.MostrarInicioUltimo() + '}';
    }

    

}
