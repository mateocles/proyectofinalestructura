package proyectofinalempresa;

public class NodoSLC {

    private Sede sede;
    private NodoSLC siguiente;
    private NodoSLC anterior;

    public NodoSLC(Sede sede, NodoSLC siguiente, NodoSLC anterior) {
        this.sede = sede;
        this.siguiente = siguiente;
        this.anterior = anterior;
    }

    public Sede getSede() {
        return sede;
    }

    public NodoSLC getSiguiente() {
        return siguiente;
    }

    public NodoSLC getAnterior() {
        return anterior;
    }

    public void setSede(Sede sede) {
        this.sede = sede;
    }

    public void setSiguiente(NodoSLC siguiente) {
        this.siguiente = siguiente;
    }

    public void setAnterior(NodoSLC anterior) {
        this.anterior = anterior;
    }

    @Override
    public String toString() {
        return "NodoLC{" + "sede=" + sede + ", siguiente=" + siguiente + ", anterior=" + anterior + '}';
    }

}
