package proyectofinalempresa;

public class ListaELC {

    private NodoELC inicio;
    private NodoELC ultimo;
    private int tamano;
    private Empleado emple;

    public ListaELC() {
        this.inicio = null;
        this.ultimo = null;
        this.tamano = 0;
        this.emple = null;
    }

    public ListaELC(NodoELC inicio, NodoELC ultimo, int tamano, Empleado emple) {
        this.inicio = inicio;
        this.ultimo = ultimo;
        this.tamano = tamano;
        this.emple = emple;
    }

    public NodoELC getInicio() {
        return inicio;
    }

    public NodoELC getUltimo() {
        return ultimo;
    }

    public int getTamano() {
        return tamano;
    }

    public Empleado getEmple() {
        return emple;
    }

    public void setInicio(NodoELC inicio) {
        this.inicio = inicio;
    }

    public void setUltimo(NodoELC ultimo) {
        this.ultimo = ultimo;
    }

    public void setTamano(int tamano) {
        this.tamano = tamano;
    }

    public void setEmple(Empleado emple) {
        this.emple = emple;
    }

    public boolean Vacia() {
        return inicio == null && ultimo == null;
    }

    public void Inser_Ini(Empleado emple) {
        if (Vacia()) {

            inicio = ultimo = new NodoELC(null, null, emple);
        } else {
            inicio = new NodoELC(inicio, null, emple);
            inicio.getSiguiente().setAnterior(inicio);
        }
        tamano++;
    }

    private void Elim_Med(int x) {
        if (!Vacia()) {
            NodoELC aux = inicio;
            while (aux != null) {
                if (aux == inicio && aux.getEmple().getCc_empleado() == x) {
                    Elim_Ini();
                    aux = inicio;
                } else {
                    if ((aux.getEmple().getCc_empleado() == x) && (inicio != aux) && (aux != ultimo)) {
                        aux.getAnterior().setSiguiente(aux.getSiguiente());
                        aux.getSiguiente().setAnterior(aux.getAnterior());
                        tamano--;
                    } else {
                        if (aux == ultimo && aux.getEmple().getCc_empleado() == x) {
                            Elim_Ult();
                        }
                    }
                }
                aux = aux.getSiguiente();
            }
        }
    }

    public void Elim_Ult() {
        if (inicio == ultimo) {
            ultimo = inicio = null;
        } else {
            ultimo = ultimo.getAnterior();
            ultimo.setSiguiente(null);
        }
        tamano--;
    }

    private void Elim_Ini() {
        if (inicio == ultimo) {
            inicio = ultimo = null;
        } else {
            inicio = inicio.getSiguiente();
            inicio.setAnterior(null);
        }
        tamano--;
    }

    public int contadorventasemple() {
        int lista = 0;
        NodoELC aux = inicio;
        while (aux != null) {
            lista += aux.getEmple().getVentas_empleado();
            aux = aux.getSiguiente();
        }
        return lista;
    }

    public String Most_Ini_Ult() {
        String lista = "";
        NodoELC aux = inicio;
        while (aux != null) {
            lista += " " + aux.getEmple() + "\n";
            aux = aux.getSiguiente();
        }
        return lista;
    }

    @Override
    public String toString() {
        return "ListaELC{" + "inicio=" + inicio + ", ultimo=" + ultimo + ", tamano=" + tamano + ", emple=" + emple + '}';
    }

}
