package proyectofinalempresa;

public class ListaSLC {

    private NodoSLC inicio;
    private NodoSLC ultimo;
    private int tamano;

    public ListaSLC() {
        this.inicio = null;
        this.ultimo = null;
        this.tamano = 0;
    }

    public NodoSLC getInicio() {
        return inicio;
    }

    public NodoSLC getUltimo() {
        return ultimo;
    }

    public int getTamano() {
        return tamano;
    }

    public void setInicio(NodoSLC inicio) {
        this.inicio = inicio;
    }

    public void setUltimo(NodoSLC ultimo) {
        this.ultimo = ultimo;
    }

    public void setTamano(int tamano) {
        this.tamano = tamano;
    }

    public boolean Vacia() {
        return inicio == null;
    }

    private void Insertar(Sede sede) {
        if (Vacia()) {
            inicio = ultimo = new NodoSLC(sede, inicio, inicio);
            inicio.setAnterior(inicio);
            inicio.setSiguiente(inicio);
            ultimo.setAnterior(ultimo);
            ultimo.setSiguiente(ultimo);
        } else {
            NodoSLC nuevo = new NodoSLC(sede, inicio, ultimo);
            inicio.setAnterior(nuevo);
            ultimo.setSiguiente(nuevo);
        }
        tamano++;
    }

    public void InsertarInicio(Sede sede) {
        Insertar(sede);
        inicio = inicio.getAnterior();
    }

    public int contadordeventasxporcuidades() {
        int totalventas = 0;
        if (!Vacia()) {
            totalventas += inicio.getSede().getEmple().contadorventasemple();
            NodoSLC aux = inicio.getSiguiente();
            while (aux != inicio) {
                totalventas += inicio.getSede().getEmple().contadorventasemple();
                aux = aux.getSiguiente();
            }
        }
        return totalventas;
    }


    public String ventasxsedes() {
        String lista = "";
        if (!Vacia()) {
            lista += inicio.getSede().getNombre_sede() + "\n";
            NodoSLC aux = inicio.getSiguiente();
            while (aux != inicio) {
                lista += inicio.getSede().getNombre_sede() +"\n";
                aux = aux.getSiguiente();
            }
        }
        return lista;
    }

    public String MostrarInicioUltimo() {
        String lista = "";
        if (!Vacia()) {
            lista += "(" + inicio.getSede() + ")";
            NodoSLC aux = inicio.getSiguiente();
            while (aux != inicio) {
                lista += "(" + aux.getSede() + ")";
                aux = aux.getSiguiente();
            }
        }
        return lista;
    }

    @Override
    public String toString() {
        return "ListaSLC{" + "inicio=" + inicio + ", ultimo=" + ultimo + ", tamano=" + tamano + '}';
    }

}
