package proyectofinalempresa;

public class ArbolBinario {

    NodoAB Raiz;
    int contadorventas = 0;

    public ArbolBinario() {
        Raiz = null;
    }

    public NodoAB getRaiz() {
        return Raiz;
    }

    public void setRaiz(NodoAB Raiz) {
        this.Raiz = Raiz;
    }

    public boolean vacio() {
        return Raiz == null;

    }

    public void insert(Ciudad d) {
        NodoAB nuevo = new NodoAB(d);
        if (vacio()) {
            Raiz = nuevo;
        } else {
            NodoAB aux = Raiz;
            NodoAB padre;
            while (true) {
                padre = aux;
                if (d.getId_ciudad() < aux.getCiudad().getId_ciudad()) {
                    aux = aux.getDere();
                    if (aux == null) {
                        padre.setDere(nuevo);
                        return;
                    }
                } else {
                    aux = aux.getIzq();
                    if (aux == null) {
                        padre.setIzq(nuevo);
                        return;
                    }
                }
                if (d.getId_ciudad() == aux.getCiudad().getId_ciudad()) {
                    return;
                }
            }
        }
    }

    public void InOrden(NodoAB r) {
        if (r != null) {
            InOrden(r.getIzq());
            contadorventas +=r.getCiudad().getSedes().contadordeventasxporcuidades() ;
            InOrden(r.getDere());
        }
    }

    public void Posorden(NodoAB r) {
        if (r != null) {
            Posorden(r.getIzq());
            Posorden(r.getDere());
            System.out.print(r.getCiudad() + "\n");
        }
    }

    public void eliminarxid(NodoAB r, int d) {
        if (r != null) {
            eliminarxid(r.getIzq(), d);
            if (r.getCiudad().getId_ciudad() == d) {
                Eliminar(r.getCiudad());
            }
            eliminarxid(r.getDere(), d);
        }
    }

    public void eliminarxnombre(NodoAB r, String d) {
        if (r != null) {
            eliminarxnombre(r.getIzq(), d);
            if (d.equals(r.getCiudad().getNombre_ciudad())) {
                Eliminar(r.getCiudad());
            }
            eliminarxnombre(r.getDere(), d);
        }
    }

    public void PreOrden(NodoAB r) {
        if (r != null) {
            System.out.print(r.getCiudad().getId_ciudad() + " " + r.getCiudad().getNombre_ciudad() + "/n");
            PreOrden(r.getIzq());
            PreOrden(r.getDere());
        }
    }

    public NodoAB Buscarid(int d) {
        NodoAB aux = getRaiz();
        while (aux.getCiudad().getId_ciudad() != d) {
            if (d > aux.getCiudad().getId_ciudad()) {
                aux = aux.getDere();
            } else {
                aux = aux.getIzq();
            }
            if (aux == null) {
                return null;
            }
        }
        return aux;
    }

    public boolean Eliminar(Ciudad d) {
        NodoAB aux = Raiz;
        NodoAB padre = Raiz;
        boolean hijoizquierdo = true;
        while (aux.getCiudad().getId_ciudad() != d.getId_ciudad()) {
            padre = aux;
            if (d.getId_ciudad() > aux.getCiudad().getId_ciudad()) {
                aux = aux.getDere();
                hijoizquierdo = false;

            } else {
                aux = aux.getIzq();
                hijoizquierdo = true;
            }
            if (aux == null) {
                return false;
            }
        }
        if (aux.getIzq() == null && aux.getDere() == null) {
            if (aux == Raiz) {
                Raiz = null;

            } else if (hijoizquierdo) {
                padre.setIzq(null);
            } else {
                padre.setDere(null);
            }
        } else if (aux.getDere() == null) {
            if (aux == Raiz) {
                Raiz = aux.getIzq();
            } else if (hijoizquierdo) {
                padre.setIzq(aux.getIzq());
            } else {
                padre.setDere(aux.getIzq());
            }
        } else if (aux.getDere() == null) {
            if (aux == Raiz) {
                Raiz = aux.getDere();
            } else if (hijoizquierdo) {
                padre.setIzq(aux.getDere());
            } else {
                padre.setDere(aux.getDere());
            }
        } else {
            NodoAB reemplazo = obtenerReemplazo(aux);
            if (Raiz == aux) {
                Raiz = reemplazo;
            } else if (hijoizquierdo) {
                padre.setIzq(reemplazo);
            } else {
                padre.setDere(reemplazo);
            }
            reemplazo.setIzq(aux.getIzq());
        }
        return false;
    }

    public NodoAB obtenerReemplazo(NodoAB nodoreemplazo) {
        NodoAB reemplazopadre = nodoreemplazo;
        NodoAB reemplazo = nodoreemplazo;
        NodoAB aux = nodoreemplazo.getDere();

        while (aux != null) {
            reemplazopadre = reemplazo;
            reemplazo = aux;
            aux = aux.getIzq();
        }
        if (reemplazo != nodoreemplazo.getDere()) {
            reemplazopadre.setIzq(reemplazo.getDere());
            reemplazo.setDere(nodoreemplazo.getDere());
        }
        return reemplazo;
    }

    @Override
    public String toString() {
        return "ArbolB{" + "Raiz=" + Raiz + '}';
    }

}
